# Git Cheatsheet


## Fundamentals
turn current directory into a Git project:
```
git init
```
check status of changes:
```
git status
```
add to staging area for Git to start tracking:
```
touch test.txt
git add test.txt
```
check differences between working directory and staging area:
```
git diff test.txt
```
permanently store changes from staging area inside repository:
```
git commit -m "This is a test"
```
list commits in chronological order:
```
git log
```
create a new repository on the command line
```
echo "# hello-world" >> README.md
git init
git add README.md
git commit -m "first commit"
git remote add origin https://github.com/shsavage/hello-world.git
git push -u origin master
```
to exclude file from Git, add file name to .gitignore:
```
touch .gitignore
cat > .gitignore
	/<name_of_file>
	#Ctrl + D to save
```
to exclude file from Git, if file has already been commited:
```
git rm <name_of_file>
```
remove given file from Git repository (after pushing):
```
git rm --cached <name_of_file>
```
or to remove all files from Git repository:
```
git rm -r --cached .
```
<br>

## Backtracking
HEAD is commit you are currently on:
```
git show HEAD
```
restore file in working directory to that of last made commit:
```
git checkout HEAD test.txt
```
unstage file from staging area:
```
git reset HEAD test.txt
```
rewind to previous commit (SHA given through git log):
```
git reset <SHA>	#first 7 characters of the SHA of previous commit
```
<br>

## Branching
to show available branches:
```
git branch
```
create new branch:
```
git branch <new_branch>
```
switch to new branch:
```
git checkout <new_branch>
```
create new branch and switch to it:
```
git checkout -b <new_branch>
```
to merge to master branch:
```
git checkout master		#switch to master branch
git merge <branch_name>
```
delete old branch:
```
git branch -d <old_branch>
```
remove remote branches:
```
git fetch --prune
```
remove origin:
```
git remote remove origin
```
to get remote branch:
```
git checkout --track origin/branch
```
<br>

## Teamwork
create new directory with hello-world.git clone:
```
git clone https://github.com/shsavage1993/hello-world.git hello-world
cd hello-world
git init
git remote -v	#to obtain name of the remote (typically origin)
```
(after some time...) bring any changes made from repository down to local copy:
```
git fetch
```
note: any changes will not be merged!
merge changes with local master branch:
```
git merge origin/master
```
create new working branch (equally could make changes straight to master branch):
```
git branch <working_branch>
git checkout <working_branch>
```
add to work branch:
```
cat > hello-world.txt
	this is a test
	#Ctrl + D to save
cat hello-world.txt		#to show contents
git add hello-world.txt
git commit -m "First hello-world commit"
git push origin work
```